import React from 'react'
import { Button } from 'antd';

import CardCategory from '../../Components/CardCategory/CardCategory';
import CardHotel from '../../Components/CardHotel/CardHotel';

function Home() {
  return (
    <div className="" >
      {/* <div className="container container_cards_category" >
        <CardCategory/>
        <CardCategory/>
        <CardCategory/>
      </div> */}

      <h1>Hotels</h1>

      <div className="container container_cards_hotel" >
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
        <CardHotel/>
      </div>
    </div>
  )
}

export default Home
