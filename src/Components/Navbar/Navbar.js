import React from 'react'

import { PageHeader } from 'antd';

function Navbar() {
  return (
    <PageHeader
      style={{
        border: '1px solid rgb(235, 237, 240)',
      }}
      onBack={() => null}
      title="Title"
      subTitle="This is a subtitle"
    />
  )
}

export default Navbar
