import React from 'react'

import HotelImg from '../../Images/palm-trees.jpg'

function CardCategory() {
  return (
    <div className="card_category" >
      <img src={HotelImg} />
      <div className="card_category__footer" >
        <p >
          Estadías
        </p>
      </div>
    </div>
  )
}

export default CardCategory
