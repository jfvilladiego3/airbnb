import React from 'react';
import logo from './logo.svg';
import './App.css';

/*Components */
import Home from './Pages/Home/Home'
import Navbar from './Components/Navbar/Navbar'

function App() {
  return (
    <div className="">
      <Navbar/>
      <Home/>
    </div>
  );
}

export default App;
