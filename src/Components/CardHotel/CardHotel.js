import React, {useState} from 'react'
import { Button } from 'antd';

import HotelImg from '../../Images/palm-trees.jpg'

function CardHotel() {
  const [isHover, setIsHover] = useState(false)

  return (
    <div className="card_hotel" 
    onMouseLeave={() => setIsHover(false)}
    onMouseOver={() => setIsHover(true)}
    >
      <img src={HotelImg}  
      
      />
      <div className={isHover ? "img_actions img_actions_show" : "img_actions" } >
        <Button type="dashed" shape="circle" icon="left"  />
        <Button type="dashed" shape="circle" icon="right"  />
      </div>
      <div className="card_hotel__footer" >
        <p >
          Estadías
        </p>
      </div>
    </div>
  )
}

export default CardHotel
